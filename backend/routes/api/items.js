const express = require("express");
const router = express.Router();
// Load models
const ItemSchema = require("../../models/Item");
const ItemValidator = require("../../validation/items");
const multer = require("multer");
const fs = require("fs");

// @route GET api/item/list
// @desc Get all item details
// @access Public
router.get("/list", (req, res) => {
  ItemSchema.find({})
    .select("title description fileName date quantity -_id")
    .then(items => {
      if (items.length > 0) {
        return res.status(200).json(items);
      } else {
        return res.status(200).json(items);
      }
    });
});

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    const errors = ItemValidator(req.body);
    if (!errors.isValid) {
      req.error = errors;
      cb(new Error(errors.errors.msg));
    }
    cb(null, "assets");
  },
  filename: function(req, file, cb) {
    const mimeTypes = [
      "image/gif",
      "image/png",
      "image/jpeg",
      "image/bmp",
      "image/webp",
      "image/x-icon",
      "image/vnd.microsoft.icon"
    ];
    if (mimeTypes.indexOf(file.mimetype) === -1) {
      req.error = { errors: { file: "Invalid FileType" }, isValid: false };
      return cb(new Error("Wrong file type"));
    }
    //cb(null, true);
    cb(null, Date.now() + "-" + file.originalname);
  }
});

// @route POST api/item/add
// @desc Get Pending Status details
// @access Public
router.post("/add", (req, res) => {
  var errors;
  const upload = multer({
    storage: storage
  }).single("file");

  upload(req, res, function(err) {
    if (req.error) {
      return res.status(400).json(req.error);
    } else if (err instanceof multer.MulterError || err) {
      return res
        .status(400)
        .json({ errors: { msg: "Please try again later", isValid: false } });
    }
    if (!req.file) {
      errors = {
        errors: {
          file: "No File Selected"
        },
        isValid: false
      };
      return res.status(400).send(errors);
    }
    const newName = req.file.filename;
    var item = new ItemSchema({
      title: req.body.title,
      description: req.body.description,
      quantity: parseInt(req.body.quantity),
      fileName: newName
    });
    item.save(function(err, doc) {
      if (err) {
        console.log(err);
        res.status(400).json({
          errors: {
            msg: "Cannot save now. Please try again.",
            isValid: false
          }
        });
      } else {
        return res.status(200).send({ doc, isValid: true });
      }
    });
  });
});

router.get("/image/:name", (req, res) => {
  //console.log(req.params.name);
  if (!req.params.name) {
    console.log(req.params.name);
    return res.status(400).send("");
  }
  fs.readFile("assets/" + req.params.name, (err, data) => {
    if (err) {
      console.log(err);
      return res.status(400).send("");
    }
    res.status(200).send(data);
  });
});

module.exports = router;
