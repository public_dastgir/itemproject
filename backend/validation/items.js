const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateItemInput(data) {
  let errors = {};
  // Convert empty fields to an empty string so we can use validator functions
  data.title = !isEmpty(data.title) ? data.title : "";
  data.description = !isEmpty(data.description) ? data.description : "";
  data.quantity = !isEmpty(data.quantity) ? data.quantity : "";
  // Title checks
  if (Validator.isEmpty(data.title)) {
    errors.title = "Title field is required. ";
  }

  if (Validator.isEmpty(data.description)) {
    errors.description = "Description field is required. ";
  }

  if (Validator.isEmpty(data.quantity)) {
    errors.quantity = "Quantity field is required. ";
  } else if (parseInt(data.quantity) <= 0) {
    errors.quantity = "Quantity is invalid. ";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
