const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");
const items = require("./routes/api/items");

const keys = require("./config/keys");

const API_PORT = 8001;
var app = express();
app.use(cors());

// this is our MongoDB database
const dbRoute = keys.mongoURI;

// connects our back end code with the database
mongoose.connect(dbRoute, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

let db = mongoose.connection;

db.once("open", () => console.log("connected to the database"));

// checks if connection with the database is successful
db.on("error", console.error.bind(console, "MongoDB connection error:"));

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));
app.disable("trust proxy");

app.use("/api/items", items);

app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));
